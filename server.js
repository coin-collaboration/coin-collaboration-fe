/* eslint-disable import/no-extraneous-dependencies */
const bs = require('browser-sync').create();
const esBuild = require('esbuild');

const start = async () => {
  try {
    const result = await esBuild.build({
      entryPoints: ['src/index.tsx'],
      bundle: true,
      minify: true,
      sourcemap: true,
      target: ['chrome58', 'firefox57', 'safari11', 'edge16'],
      outfile: 'build/main.js',
      incremental: true,
      define: {
        'process.env.NODE_ENV': '"development"',
      },
    });

    bs.init({
      server: './build',
    });

    bs.watch('src/**/*.tsx').on('change', async () => {
      await result.rebuild();
      bs.reload();
    });
  } catch (ex) {
    process.exit(1);
  }
};

start();
