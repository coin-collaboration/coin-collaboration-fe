import * as React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

declare const document: Document;

ReactDOM.render(<App />, document.getElementById('root'));
